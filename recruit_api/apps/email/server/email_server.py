from django.core.mail import send_mail
from django.conf import settings
from django.http import BadHeaderError, HttpResponse, HttpResponseRedirect
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from django.core.mail import send_mail, EmailMessage
from email import encoders
from recruit_api.settings.email_server import EMAIL_HOST_USER


class EmailServer:

    def send(self, subject, message, recipients, uploaded_file):

        email = EmailMessage(
            subject,
            message,
            EMAIL_HOST_USER,
            recipients,
        )

        if uploaded_file:
            file = MIMEBase('application', "octet-stream")
            file.set_payload(uploaded_file.read())
            encoders.encode_base64(file)
            file.add_header('Content-Disposition',
                            'attachment',
                            filename=uploaded_file.name)

            email.attach(file)

        if email:
            try:
                return email.send()
            except BadHeaderError:
                return "Invalid header found."

        # return send_mail(subject, message, settings.EMAIL_HOST_USER, recipients)
