from rest_framework import serializers
from recruit_api.apps.client.models import Client
from recruit_api.apps.client.models.client import Contact


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        exclude = ('created', 'modified')


class ContactSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contact
        fields = '__all__'


class ClientListSerializer(serializers.ModelSerializer):
    contact = ContactSerializer(read_only=True, many=True)

    class Meta:
        model = Client
        exclude = ('created', 'modified')
        # fields = '__all__'
        # fields = ('id', 'contact', 'name', 'location', 'founded_year', 'website', 'employee_count', 'logo_url', 'company_info',
        #           'competitors', 'selling_points', 'products', 'description', 'fee')
