from django.db import models
from django.core.validators import (MinLengthValidator, MinValueValidator)
from model_utils.models import TimeStampedModel
from recruit_api.apps.client.constants import (EMPLOYEE_COUNT_CHOICES, DEFAULT_EMPLOYEE_COUNT)


class RecruitMinLengthValidator(MinLengthValidator):
    def compare(self, a, b):
        return int(a) < int(b)

    def clean(self, x):
        return len(str(x))


class Contact(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"

    def __str__(self):
        return '{} - {}'.format(self.name, self.email)


class Client(TimeStampedModel):
    name = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    founded_year = models.IntegerField(validators=[MinValueValidator(0), RecruitMinLengthValidator(4)])
    website = models.URLField(max_length=500)
    employee_count = models.IntegerField(choices=EMPLOYEE_COUNT_CHOICES, default=DEFAULT_EMPLOYEE_COUNT)
    logo_url = models.URLField(max_length=500, blank=True, null=True)
    company_info = models.TextField(blank=True, null=True)
    competitors = models.TextField(blank=True, null=True)
    selling_points = models.TextField(blank=True, null=True)
    products = models.TextField(blank=True, null=True)
    description = models.TextField()
    contact = models.ManyToManyField(Contact, blank=True)
    fee = models.IntegerField(null=True, blank=True)

    class Meta:
        app_label = "client"
        ordering = ['name']
        verbose_name_plural = "Clients"


    def __str__(self):
        return self.name
